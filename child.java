package Inheritances;

public class child extends parent{
	int num=40;
	
	public void display() {
		System.out.println("This is from child class");
	}
	
	public void childDetail() {
		System.out.println(super.num);
		System.out.println(super.parentNum);
		super.display();
	}
}
