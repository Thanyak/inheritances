package Inheritances;

public class CalculatorDemo {

	public static void main(String[] args) {
		BasicCalculator bc= new BasicCalculator();
		ScientificCalculator sc=new ScientificCalculator();
		System.out.println(bc.addNum(5, 8));
		System.out.println(bc.mulNum(9, 5));
		
		System.out.println(sc.power(5, 3));

	}

}
